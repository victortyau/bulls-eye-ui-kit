//
//  AboutViewController.swift
//  BullsEyeUIKit
//
//  Created by Victor Tejada Yau on 5/2/20.
//  Copyright © 2020 Garatge Labs. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func close() {
        dismiss(animated: true, completion: nil)
    }
    
}
