//
//  ViewController.swift
//  BullsEyeUIKit
//
//  Created by Victor Tejada Yau on 4/24/20.
//  Copyright © 2020 Garatge Labs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var currentValue: Int = 0
    var targetValue: Int = 0
    var score = 0
    var round = 1
    
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var targetLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var roundLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let roundedValue = slider.value.rounded();
        currentValue = Int(roundedValue)
        targetValue = Int.random(in: 1...100)
        startOver()
    }
    
    @IBAction func showAlert() {
        
        let difference = abs(targetValue - currentValue)
        var points = 100 - difference
        
        score += points
        
        let title: String
        
        if difference == 0 {
            title = "Perfect!"
            points += 100
        }else if difference < 5 {
            title = "You almost had it!"
            if difference == 1 {
                points += 50
            }
        }else if difference < 10 {
            title = "Pretty good!"
        }else {
            title = "Not even close..."
        }
        
        print("#padentro 🍵")
        let message = "You scored \(points) points"
//        let message =  "The value of the slider is now: \(currentValue)" +
//        "\n The target value is: \(targetValue)" +
//        "\n The difference is \(difference)"
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Awesome", style: .default, handler: {
            action in
            self.startNewRound()
        })
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
        ///startNewRound()
    }
    
    @IBAction func sliderMoved(_ slider: UISlider) {
        print("The value of the sldier is now: \(slider.value) 💰")
        let roundedValue = slider.value.rounded();
        currentValue = Int(roundedValue)
    }
    
    func startNewRound() {
        round += 1
        targetValue = Int.random(in: 1...100)
        currentValue = 50
        slider.value = Float(currentValue)
        updateLabels()
    }
    
    func updateLabels() {
        targetLabel.text = String(targetValue)
        scoreLabel.text = String(score)
        roundLabel.text = String(round)
    }
    
    @IBAction func startOver() {
        score = 0
        round = 0
        startNewRound()
    }

}

